#include <stdio.h>
#include <stdlib.h>

struct nodo{
	int info;
	struct nodo *sig;
};

struct nodo	*raiz = NULL;

void reemplazar(struct nodo *pila, int viejo, int nuevo){
	if(pila!= NULL){
		if(pila->info == viejo){
			pila->info = nuevo;
		}
		else{
			reemplazar(pila->sig,viejo,nuevo);
		}
	}
	else{
		printf("No hay coincidencias.\n");
	}
}

void insertar(int x){
struct nodo *nuevo;
	nuevo = malloc(sizeof(struct nodo));
	nuevo->info = x;
	if(raiz	== NULL){
		raiz = nuevo;
		nuevo->sig == NULL;
	}
	else{
		nuevo->sig = raiz;
		raiz = nuevo;
	}
}
void imprimir(){
	struct nodo *reco=raiz;
	printf("Lista completa:\n");
	while(reco!=NULL){
		printf("%i ",reco->info);
		reco=reco->sig;
	}
	printf("\n");
}
    
int main(){
	insertar(10);
	insertar(38);
	insertar(21);
	imprimir();
	printf("Reemplazando elemento de la pila.\n");
	reemplazar(raiz,10,20);
	imprimir();
	printf("Reemplazando elemento de la pila.\n");
	reemplazar(raiz,30,15);
	imprimir();
	return 0;
}